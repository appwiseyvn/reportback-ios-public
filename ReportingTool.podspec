Pod::Spec.new do |s|

s.platform = :ios
s.ios.deployment_target = '10.0'
s.name = "ReportBack"
s.summary = "Bug reporting and user feedback gathering tool"
s.requires_arc = true
s.version = "0.0.3"
s.license = { :type => "MIT", :file => "LICENSE" }
s.author = { "Appwise" => "yerevan@appwise.be" }
s.homepage = "https://appwise.be"
s.source = { :git => "bitbucket.org/appwiseyvn/reportback-ios-public.git", :tag => "#{s.version}"}
s.framework = "CoreLocation"
s.framework = "UIKit"
s.vendored_framework = "ReportBack.framework"
end

